package com.huage.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.huage.service.HelloService;

@Service
public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(String name) {
        return "Hello"+name;
    }
}
